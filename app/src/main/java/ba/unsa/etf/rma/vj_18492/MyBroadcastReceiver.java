package ba.unsa.etf.rma.vj_18492;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_SEND)) {
            Intent intent1 = new Intent(context.getApplicationContext(), MainActivity.class);
            intent1.putExtra("Search", intent.getDataString());
            context.startActivity(intent1);
        }
    }

}