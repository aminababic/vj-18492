package ba.unsa.etf.rma.vj_18492.models;

import java.util.ArrayList;
import java.util.Arrays;

public class MoviesModel {
    private static ArrayList<Movie> movies = new ArrayList<>(
            Arrays.asList(
                    new Movie("The Lost One", "Neki overview",
                            "11.11.1918", "https://github.com/",
                            "action"),
                    new Movie("Forgotten Agent", "nesto",
                            "11.10.2019", "https://c2.etf.unsa.ba",
                            "drama"),
                    new Movie("Last Wish", "...",
                            "20.2.2020", "https://youtube.com/",
                            "romantic"),
                    new Movie("The Second Lost One", "Neki overview",
                            "11.11.1918", "https://bitbucket.com/",
                            "romantic"),
                    new Movie("The Third Lost One", "Neki overview",
                            "11.11.1918", "https://github.com/",
                            "romantic")
            )
    );

    public static ArrayList<Movie> getMovies() {
        return movies;
    }
}
