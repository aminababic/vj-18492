package ba.unsa.etf.rma.vj_18492.presenters;

import java.util.List;

import ba.unsa.etf.rma.vj_18492.Contract;
import ba.unsa.etf.rma.vj_18492.interactors.MovieListInteractor;
import ba.unsa.etf.rma.vj_18492.models.Movie;

public class MovieListPresenter implements Contract.Presenter {
    private MovieListInteractor movieListInteractor;

    public MovieListPresenter() {
        movieListInteractor = new MovieListInteractor();
    }
    public List<Movie> getMovies(){
        return movieListInteractor.get();
    }
}
