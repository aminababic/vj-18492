package ba.unsa.etf.rma.vj_18492.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import ba.unsa.etf.rma.vj_18492.R;
import ba.unsa.etf.rma.vj_18492.models.Movie;

public class MovieListAdapter extends ArrayAdapter<Movie> {
    private int resource;

    public MovieListAdapter(@NonNull Context context, int resource, @NonNull List<Movie> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if(convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater;
            layoutInflater = (LayoutInflater)getContext().getSystemService(inflater);
            layoutInflater.inflate(resource,newView,true);
        } else {
            newView = (LinearLayout)convertView;
        }
        Movie movie = getItem(position);
        if (movie != null) {
            TextView textView = newView.findViewById(R.id.listItemTextView);
            ImageView imageView = newView.findViewById(R.id.imageView);
            textView.setText(movie.getTitle() + "\n" + movie.getGenre());
            switch (movie.getGenre()) {
                case "action":
                    imageView.setImageResource(R.drawable.action);
                    break;
                case "drama":
                    imageView.setImageResource(R.drawable.drama);
                    break;
                case "romantic":
                    imageView.setImageResource(R.drawable.romantic);
                    break;
            }
        }
        return newView;
    }

    public Movie getMovie(int position) {
        return getItem(position);
    }
}
