package ba.unsa.etf.rma.vj_18492.interactors;

import java.util.List;

import ba.unsa.etf.rma.vj_18492.models.Movie;
import ba.unsa.etf.rma.vj_18492.models.MoviesModel;

public class MovieListInteractor {
    public List<Movie> get(){
        return MoviesModel.getMovies();
    }
}
