package ba.unsa.etf.rma.vj_18492;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.vj_18492.adapters.MovieListAdapter;
import ba.unsa.etf.rma.vj_18492.models.Movie;
import ba.unsa.etf.rma.vj_18492.presenters.MovieListPresenter;

public class MainActivity extends AppCompatActivity implements Contract.View{
    private Button button;
    private ListView listView;
    private EditText editText;
    private ArrayList<String> entries;
    private ArrayAdapter<String> adapter;
    private MovieListAdapter movieListAdapter;
    private MovieListPresenter movieListPresenter;
    private AdapterView.OnItemClickListener listItemCLickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent movieDetailIntent = new Intent(MainActivity.this, MovieDetailActivity.class);
                    Movie movie = movieListAdapter.getMovie(position);
                    movieDetailIntent.putExtra("title", movie.getTitle());
                    movieDetailIntent.putExtra("Movie", movie);
                    MainActivity.this.startActivity(movieDetailIntent);
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.button);
        button.setActivated(false);
        listView = (ListView) findViewById(R.id.listView);
        editText = (EditText) findViewById(R.id.editText);
        entries = new ArrayList<>();
        movieListPresenter = new MovieListPresenter();
        movieListAdapter = new MovieListAdapter(this,R.layout.list_element,movieListPresenter.getMovies());

        adapter = new ArrayAdapter<String>(this, R.layout.list_element, R.id.listItemTextView, entries);
        listView.setAdapter(movieListAdapter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(!text.equals("")) {
                    entries.add(0, text);
                    adapter.notifyDataSetChanged();
                    editText.setText("");
                    System.out.println(text);
                }
            }
        });
        listView.setOnItemClickListener(listItemCLickListener);
    }
}
