package ba.unsa.etf.rma.vj_18492;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import ba.unsa.etf.rma.vj_18492.models.Movie;

public class MovieDetailActivity extends AppCompatActivity {

    private TextView textView1;
    private TextView textView2;
    private TextView textView3;
    private TextView textView4;
    private TextView textView5;
    private ListView listView;
    private Movie movie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        textView1 = (TextView)findViewById(R.id.textView);
        textView2 = (TextView)findViewById(R.id.textView2);
        textView3 = (TextView)findViewById(R.id.textView3);
        textView4 = (TextView)findViewById(R.id.textView4);
        textView5 = (TextView)findViewById(R.id.textView5);
        listView = (ListView)findViewById(R.id.listView);
        String title = getIntent().getStringExtra("title");
        movie = (Movie) getIntent().getSerializableExtra("Movie");
        textView1.setText(movie.getTitle());
        textView2.setText(movie.getGenre());
        textView3.setText(movie.getHomepage());
        textView4.setText(movie.getOverview());
        textView5.setText(movie.getReleaseDate());
        textView3.setClickable(true);

        textView3.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             System.out.println("HIHI");
                                             String url = movie.getHomepage();
                                             Intent i = new Intent(Intent.ACTION_VIEW);
                                             i.setData(Uri.parse(url));
                                             startActivity(i);
                                         }
                                     }
            );

    }
}
